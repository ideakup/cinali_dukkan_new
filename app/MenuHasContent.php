<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuHasContent extends Model
{
    protected $table = 'menu_has_content';
}
