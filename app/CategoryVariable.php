<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryVariable extends Model
{
    protected $table = 'categoryvariable';
}
