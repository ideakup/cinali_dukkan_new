let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
*/

mix.combine([
	/*
		'node_modules/bootstrap/dist/css/bootstrap.min.css',
		'node_modules/font-awesome/css/font-awesome.min.css',
		'node_modules/ladda/dist/ladda.min.css',
		'node_modules/ladda/dist/ladda-themeless.min.css',
		'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
		'node_modules/jquery-colorbox/example2/colorbox.css',

		'node_modules/select2/dist/css/select2.css',

		'node_modules/fullcalendar/dist/fullcalendar.css',

		'resources/assets/css/cinali-icon.css',
		'resources/assets/css/ion.rangeSlider.css',
	    'resources/assets/css/ion.rangeSlider.skinFlat.css',
	    'resources/assets/css/custom.css',
	    'resources/assets/css/fullcalendarcustom.css',
    */
    
	'node_modules/font-awesome/css/font-awesome.min.css',
    'resources/assets/css/custom.css',
    'resources/assets/css/aimeos_custom.css',
], 'public/css/app.css');


mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/fonts/cinali', 'public/fonts');
mix.copy('resources/assets/fonts/cinali-icon', 'public/fonts');
mix.copy([
	'node_modules/font-awesome/fonts/',
], 'public/fonts', false);