<!DOCTYPE html>
<html lang="tr" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="content-language" content="tr">
	@yield('aimeos_header')

	<title>Aimeos on Laravel</title>

    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	@yield('aimeos_styles')
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

	@include('partials.header')

	<div class="container">
		<div class="row">
            <div class="col-md-12 text-center">
            	<img src="{{ asset('images/headerimg.jpeg') }}" alt="" style="width: 100%;" />
            </div>
        </div>
    </div>
    <div class="breadcrumbSeperator"></div>
	
    <div class="container">
		<div class="row">
			
				@yield('aimeos_nav')
			
			
				<!-- @-yield('aimeos_stage') -->
				@yield('aimeos_body')
			
		</div>
	</div>

    <div class="col-xs-12">

		@yield('content')
	</div>


	@include('partials.footer')

	<!-- Scripts -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	@yield('aimeos_scripts')
	</body>
</html>