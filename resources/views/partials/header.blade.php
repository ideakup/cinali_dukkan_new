@php
        
    use App\Menu;
    $lang = 'tr';
    $topmenus = Menu::where('position', '!=', 'aside')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

@endphp

<nav class="navbar navbar-default">
    <div class="container padding-0 headerLogoContainer">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ asset('images/logo/cin-aliLogo2.png') }}" class="img-responsive" alt="Cin Ali"></a>
            <div class="navbar-right">
                @yield('aimeos_head')
            </div>
        </div>
    </div>
    <div class="collapse navbar-collapse padding-0" id="bs-example-navbar-collapse-1">
        <div class="main-fix-menu">
            <div class="container">
                <style type="text/css">
                    .dropdown-toggle .fa-caret-down::before{
                        content: "";
                    }
                </style>
                <ul class="nav navbar-nav">
                    @php 
                        $i = 0; 
                    @endphp
                    @foreach ($topmenus as $menuitem)
                        @include('partials.headermenu')
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="menuSpacer" style="height: 212px; display:none;"></div>