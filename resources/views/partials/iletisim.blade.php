	<style>
		.form-control{
			height: 39px;
			border-radius: 2px;
		}

		textarea.form-control {
		    height: 93px;
		}

		.iletisim p{
			text-align: left;
			font-size: 14px;
		}

		.iletisimHead{
			background-image: url('{{ url('images/pul.png') }}');
		    margin-bottom: 15px;
		    background-position: right center;
		    background-repeat: no-repeat;
		    background-size: contain;
		}

		.iletisimHead h2{
			color: #FFF; margin: 18px 0 12px; line-height: 30px;
		}
	</style>

	<div class="col-md-12 m-b-20">
		
		<div class="col-md-3 iletisimFormCol" style="background-color: #e9f4f9;">
			<h1>Cin Ali</h1>
			{!! $menu->content[0]->variableLang(Request::segment(1))->content !!}
		</div>

		<div class="col-md-9 iletisimFormCol" style="background-color: #e9f4f9;">
			<form id="iletisimform" class="contact_form" method="post" action="{{url('iletisim_mail')}}" role="form">
                {{ csrf_field() }}
                <div class="form-body">
                	<div class="col-md-12">
						<p style="margin-top: 37px;">İletişim Formu</p>
					</div>
                	<div class="col-md-6">
                        <div class="form-group">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Adı Soyadı" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" id="email" name="email" class="form-control" placeholder="E-Posta" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="tel" id="tel" name="tel" class="form-control" placeholder="Cep Tel" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                            <textarea id="message" name="message" class="form-control" rows="3" placeholder="Mesaj" required></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group pull-right">
                            <button type="submit" id="form_submit" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> Gönder </span><span class="ladda-spinner"></span></button>
                            <button type="reset" class="btn ladda-button" data-color="second" data-size="s" style="outline: 0;"><span class="ladda-label"><i class="fa fa-eraser" aria-hidden="true"></i> Temizle </span><span class="ladda-spinner"></span></button>
                        </div>
                    </div>
                </div>
			</form>
		</div>

	</div>
	
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #fed630;"> <h2>Müze</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[1]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #feb555;"> <h2>Vakıf</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[2]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #be6f7e;"> <h2>Kütüphane</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[3]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	<div class="col-md-3 iletisim m-b-20">
		<div class="col-md-12 iletisimHead" style="background-color: #c6e04e;"> <h2>Filli Bahçe</h2> </div>
		<div class="col-md-12">
			{!! $menu->content[4]->variableLang(Request::segment(1))->content !!}
		</div>
	</div>
	
	<div class="col-md-12">
		<iframe src="{{URL::to('tr/map')}}" width="100%" height="300" frameborder="0"></iframe>
	</div>
