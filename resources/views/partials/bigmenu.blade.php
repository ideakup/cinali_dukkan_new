<div class="row home-main hidden-xs">
    <div class="col-xs-12">
        <a href="{{ url($lang.'/muzesi') }}" class="button red"><span>MÜZESİ</span>MÜZESİ</a>
        <a href="{{ url($lang.'/vakfi') }}" class="button orange"><span>VAKFI</span>VAKFI</a>
        <a href="{{ url($lang.'/filli-bahcesi') }}" class="button green"><span>FİLLİ BAHÇESİ</span>FİLLİ BAHÇESİ</a>
        <a href="{{ url($lang.'/kutuphanesi') }}" class="button purple"><span>KÜTÜPHANESİ</span>KÜTÜPHANESİ</a>
        <a href="{{ url($lang.'/etkinlikler') }}" class="button blue"><span>ETKİNLİKLERİ</span>ETKİNLİKLERİ</a>
        <a href="http://www.cinali.com.tr" target="_blank" class="button yellow"><span>DÜKKANI</span>DÜKKANI</a>
    </div>
</div>