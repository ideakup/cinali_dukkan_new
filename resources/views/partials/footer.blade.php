@php
        
    use App\Menu;
    $lang = 'tr';
    $menuall = Menu::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

@endphp

<div class="footer-main">
	<div class="container">
		<div class="row">
			
			@foreach ($menuall as $menuitem)
				@if ($loop->index < 6)
					@if ($loop->index % 3 == 0)
						<div class="col-sm-6 padding-0 foot-menu">
					@endif
							<div class="col-sm-4" @if ($loop->index == 0) style="border: 0;" @endif>
								<h4><a href="{{url($lang.'/'.$menuitem->variableLang($lang)->slug)}}">{{ $menuitem->variableLang($lang)->menutitle }}</a></h4>
								
								@if ($menuitem->subMenu->count() >= 1)
									<ul>
										@foreach ($menuitem->subMenu as $submenuitem)
											<li>
												<!--
													<a href="{{ url($lang.'/'.$submenuitem->variableLang($lang)->slug) }}">{{ $submenuitem->variableLang($lang)->menutitle }}</a><br>
												-->

												@if ($submenuitem->type == 'content' || $submenuitem->type == 'list' || $submenuitem->type == 'photogallery' || $submenuitem->type == 'photogallery')
									                <a href="{{ url($lang.'/'.$submenuitem->variableLang($lang)->slug) }}">
									                    {{ $submenuitem->variable->menutitle }} {{-- $submenuitem->subMenuTop->count().' + '.$i --}}
									                </a>
									            @elseif ($submenuitem->type == 'link')
									                @php
									                    $linkUrl = '';
									                @endphp
									                @if (starts_with(json_decode($submenuitem->variableLang($lang)->stvalue)->link, '#'))
									                    @php
									                        $linkUrl = $submenuitem->topMenu->variableLang($lang)->slug.''.json_decode($submenuitem->variableLang($lang)->stvalue)->link;
									                    @endphp
									                @else
									                    @php
									                        $linkUrl = json_decode($submenuitem->variableLang($lang)->stvalue)->link;
									                    @endphp
									                @endif

									                <a href="{{ $linkUrl }}" target="_{{ json_decode($submenuitem->variableLang($lang)->stvalue)->target }}">
									                    {{ $submenuitem->variable->menutitle }} 
									                </a>
									            @endif
											</li>

										@endforeach
									</ul>
								@endif
														
							</div>
					@if ($loop->index % 3 == 2)
						</div>
					@endif
				@endif
			@endforeach
		</div>
		<div class="row">
			<div class="col-sm-6 padding-0">
				<div class="col-sm-3" style="padding-top: 10px;"><img src="{{asset('images/logo.png')}}" class="img-responsive" alt=""></div>
				<div class="col-sm-9" style="font-size: 10px; padding-top: 16px;">					
					Bülten Sok. No: 32 06680 Kavaklıdere / ANKARA
					<br>
					T: 0 312 426 80 42 F: 0 312 426 83 54 cinfo@cinali.com.tr
				</div>				
			</div>
			<div class="col-sm-6" style="padding-top: 10px;">
				
				<div class="col-sm-5" style="text-align: right; font-size: 12px;">
					Cin Ali E-Bültene Kayıt Olun!<br>Yeniliklerden Haberdar Olun!
				</div>
				<div class="col-lg-7">
				    <div class="input-group">
						<input type="text" class="form-control" placeholder="E-posta adresinizi giriniz..." style="border-radius: 0; border: none; height: 30px;    font-size: 12px;">
						<span class="input-group-btn">
							<button type="submit" id="form_submit" class="btn ladda-button" data-color="first" data-style="expand-right" data-size="s" style="outline: 0; font-size: 12px;"><span class="ladda-label"><i class="fa fa-paper-plane" aria-hidden="true"></i> Kayıt Ol! </span><span class="ladda-spinner"></span></button>
						</span>
					</div>
				</div>
				<!--<div class="col-sm-3 padding-0"><img src="{{asset('images/visa.png')}}" style="margin-top: 16px;" class="img-responsive" alt=""></div>-->
			</div>
		</div>		
	</div>
</div>