<style type="text/css">
    .carousel-caption{
        font-size: 20px;
    }
</style>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
        <ol class="carousel-indicators">
            @foreach ($menu->slider as $slide)
                <li data-target="#carousel-example-generic" data-slide-to="{{ $loop->index }}" @if($loop->first) class="active" @endif></li>
            @endforeach
        </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @foreach ($menu->slider as $slide)
            <div class="item @if ($loop->first) active @endif">
                <img src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$slide->variable->image_url) }}"  style="width: 100%; margin: auto;">
                <div class="carousel-caption">{{-- $slide->title --}}</div>
            </div>
        @endforeach
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <!--<button type="button" class="slider-prev"><span aria-label="Önceki">›</span></button>-->
        <span class="slider-prev" aria-hidden="true"></span>
        <span class="sr-only">Önceki</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <!--<button type="button" class="slider-next"><span aria-label="Sonraki">›</span></button>-->
        <span class="slider-next" aria-hidden="true"></span>
    <span class="sr-only">Sonraki</span>
    </a>
</div>
