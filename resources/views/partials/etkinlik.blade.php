<div class="etkinlikSeperator"> </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="contentHeader">Cin Ali Bu Ay Ne Yapıyor?</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row padding-bottom-20">
        <div class="owl-carousel owl-theme">
            @foreach ($calendarSlide as $event)
                <div class="item col-xs-12">
                    <a href="{{ url($lang.'/etkinlikler/'.$event->id) }}">
                        <img class="img-responsive" src="{{ (is_null($event->variable->image_name)) ? url(env('APP_UPLOAD_PATH_V3').'default_event.jpg') : url(env('APP_UPLOAD_PATH_V3').'small/'.$event->variable->image_name)}}" alt="">
                    </a>
                    <h5 class="event-header text-center">
                        @if (!is_null($event->type))
                            @if ($event->type == 'other')
                                Diğer
                            @elseif ($event->type == 'kutuphaneetkinligi')
                                Kütüphane Etkinliği
                            @elseif ($event->type == 'muzeetkinligi')
                                Müze Etkinliği
                            @elseif ($event->type == 'filmgosterimi')
                                Film Gösterimi
                            @elseif ($event->type == 'soylesi')
                                Söyleşi
                            @elseif ($event->type == 'konser')
                                Konser
                            @endif
                        @endif
                        
                    </h5>
                </div>
            @endforeach
        </div>
    </div>
</div>