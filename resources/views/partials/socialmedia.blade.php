<a id="topBtn"></a>

<div class="social_media_container">
	<a href="https://www.facebook.com/CinAliBizim/" target="_blank">
		<svg version="1.1" class="svgicon svgicon_fb" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 28 28" style="enable-background:new 0 0 28 28;" xml:space="preserve">
			<g>
				<path d="M14.002,1.695C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.312,12.305
					c6.797,0,12.309-5.51,12.309-12.305C26.311,7.204,20.799,1.695,14.002,1.695"/>
				<path style="fill:none;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M14.002,1.695
					C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.312,12.305c6.797,0,12.309-5.51,12.309-12.305
					C26.311,7.204,20.799,1.695,14.002,1.695"/>
				<path style="fill:#FFFFFF;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M12.058,21.178h2.959
					V14h2.285l0.279-2.53h-2.564V9.899c0-0.559,0.232-0.607,0.514-0.607h2.012V6.831l-2.378-0.008c-2.638,0-3.106,1.835-3.106,3.009
					v1.639H10.4V14h1.657V21.178z"/>
			</g>
		</svg>
	</a>
	<a href="https://www.instagram.com/cinalibizim/" target="_blank">
		<svg version="1.1" class="svgicon svgicon_in" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 28 28" style="enable-background:new 0 0 28 28;" xml:space="preserve">
			<g>
				<path d="M14,1.695C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.311,12.305
					S26.311,20.795,26.311,14C26.311,7.204,20.799,1.695,14,1.695"/>
				<path style="fill:none;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M14,1.695
					C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.311,12.305S26.311,20.795,26.311,14
					C26.311,7.204,20.799,1.695,14,1.695"/>
				<path style="fill:#FFFFFF;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M18.405,6.822H9.594
					c-1.531,0-2.775,1.244-2.775,2.775v2.93v5.877c0,1.529,1.244,2.773,2.775,2.773h8.812c1.53,0,2.776-1.244,2.776-2.773v-5.877v-2.93
					C21.182,8.066,19.936,6.822,18.405,6.822"/>
				<path style="stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M11.646,12.308
					c0.528-0.73,1.385-1.209,2.354-1.209s1.826,0.479,2.354,1.209c0.344,0.477,0.551,1.061,0.551,1.692c0,1.599-1.304,2.9-2.904,2.9
					S11.097,15.6,11.097,14C11.097,13.369,11.303,12.785,11.646,12.308"/>
				<path style="fill:none;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M17.779,9.521
					c0.092-0.126,0.24-0.209,0.408-0.209s0.315,0.083,0.408,0.209c0.059,0.083,0.095,0.184,0.095,0.293
					c0,0.277-0.227,0.502-0.503,0.502c-0.278,0-0.503-0.225-0.503-0.502C17.685,9.704,17.721,9.604,17.779,9.521"/>
				
			</g>
		</svg>
	</a>
	<a href="https://twitter.com/CinAliBizim" target="_blank">
		<svg version="1.1" class="svgicon svgicon_tw" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 28 28" style="enable-background:new 0 0 28 28;" xml:space="preserve">
			<g>
				<path d="M14,1.695C7.202,1.695,1.688,7.204,1.688,14c0,6.795,5.514,12.305,12.312,12.305
					c6.798,0,12.312-5.51,12.312-12.305C26.312,7.204,20.799,1.695,14,1.695"/>
				<path style="stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M14,1.695
					C7.202,1.695,1.688,7.204,1.688,14c0,6.795,5.514,12.305,12.312,12.305c6.798,0,12.312-5.51,12.312-12.305
					C26.312,7.204,20.799,1.695,14,1.695"/>
				<path style="fill:#FFFFFF;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M8.885,11.646
					c0.003,0.675,0.017,1.353,0.021,2.032c0,0.73,0.002,1.459,0.002,2.189c0,4.086,3.656,5.242,4.356,5.242
					c0.702,0,3.449,0.068,3.945,0.068s1.92-0.133,1.92-1.598c0-1.463-1.01-1.988-1.691-1.988h-2.729c-1.939,0-2.25-0.873-2.25-1.68
					c0-0.387,0.024-1.018,0.047-1.792h4.558c0.682,0,1.98-0.283,1.98-1.552c0-1.267-0.598-2.097-1.404-2.097h-5.077
					c-0.007-1.828-0.123-3.648-1.881-3.648c-0.726,0-1.315,0.302-1.563,1.056c-0.327,0.99-0.241,2.147-0.236,3.178
					C8.884,11.252,8.885,11.45,8.885,11.646"/>
			</g>
		</svg>
	</a>
	<a href="https://www.youtube.com/channel/UCNeYJPl5lO8dKf3tojz6L2g" target="_blank">
		<svg version="1.1" class="svgicon svgicon_yt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 28 28" style="enable-background:new 0 0 28 28;" xml:space="preserve">
			<g>
				<path d="M14,1.695C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.311,12.305
					c6.798,0,12.311-5.51,12.311-12.305C26.311,7.204,20.798,1.695,14,1.695"/>
				<path style="fill:none;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M14,1.695
					C7.201,1.695,1.689,7.204,1.689,14c0,6.795,5.512,12.305,12.311,12.305c6.798,0,12.311-5.51,12.311-12.305
					C26.311,7.204,20.798,1.695,14,1.695"/>
				<path style="fill:#FFFFFF;stroke:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M21.037,11.084
					c0,0-0.141-1.005-0.571-1.447c-0.546-0.58-1.157-0.584-1.438-0.617c-2.011-0.147-5.025-0.147-5.025-0.147h-0.007
					c0,0-3.015,0-5.024,0.147c-0.28,0.033-0.893,0.037-1.438,0.617c-0.431,0.442-0.571,1.447-0.571,1.447s-0.144,1.18-0.144,2.36v1.106
					c0,1.18,0.144,2.359,0.144,2.359s0.141,1.004,0.571,1.447c0.546,0.58,1.264,0.561,1.583,0.621c1.149,0.111,4.883,0.148,4.883,0.148
					s3.019-0.006,5.029-0.152c0.28-0.035,0.892-0.037,1.438-0.617c0.431-0.443,0.571-1.447,0.571-1.447s0.145-1.18,0.145-2.359v-1.106
					C21.182,12.264,21.037,11.084,21.037,11.084 M12.518,15.891l-0.001-4.096l3.88,2.056L12.518,15.891z"/>
			</g>
		</svg>
	</a>
</div>
<div class="social_media_animation">
	Heey! Ben de burdayım!
</div>
