@if ($menu->slidertype == "image" && $menu->variableLang(Request::segment(1))->stvalue != null)
	<div class="container">
		<div class="row">
            <div class="col-md-12 text-center">
            	<img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $menu->variableLang(Request::segment(1))->stvalue }}" alt="" style="width: 100%;" />
            </div>
        </div>
    </div>
@else
	@include('animations.ucakbulut')
@endif